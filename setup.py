from setuptools import setup


'''
https://docs.python.org/3.6/distutils/setupscript.html?highlight=setup%20cfg
https://setuptools.readthedocs.io/en/latest/setuptools.html#configuring-setup-using-setup-cfg-files
https://docs.python.org/2/distutils/sourcedist.html#commands
'''
setup()
