from django import template
from django.shortcuts import render
from django.template.loader import get_template
from django.template.defaultfilters import truncatewords_html


register = template.Library()


# @register.inclusion_tag('blog_intro.html', takes_context=True)
@register.simple_tag(takes_context=True)
def get_intro(context, blog):
    return blog.specific.get_intro()


# @register.simple_tag(takes_context=True)
# def get_blog_summary(context, blog, counter):
    # index = blog.get_blog_index().specific
    # blog.specific.set_template_type('post')
    # template = get_template(blog.specific.get_template(context['request']))
    # return template.render(
        # {'index': index, 'blog': blog.specific, 'summary': True},
        # context['request']
#     )
