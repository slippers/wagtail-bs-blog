from django.db import models
from django.utils.translation import gettext_lazy as _
from django_extensions.db.fields import AutoSlugField
from django.core.exceptions import ValidationError
from wagtail.models import Orderable
from wagtail.admin.panels import FieldPanel
from modelcluster.fields import ParentalKey
from .utils import unique_slugify


class BlogCategory(models.Model):

    class Meta:
        ordering = ['name']
        verbose_name = _("Blog Category")
        verbose_name_plural = _("Blog Categories")

    name = models.CharField(
        max_length=80, unique=True, verbose_name=_('Category Name'))
    slug = AutoSlugField(populate_from='name', editable=True)
    parent = models.ForeignKey(
        'self', blank=True, null=True, related_name="children",
        help_text=_(
            'Categories, unlike tags, can have a hierarchy. You might have a '
            'Jazz category, and under that have children categories for Bebop'
            ' and Big Band. Totally optional.'),
        on_delete=models.CASCADE,
    )
    description = models.CharField(max_length=500, blank=True)

    panels = [
        FieldPanel('name'),
        FieldPanel('slug'),
        FieldPanel('parent'),
        FieldPanel('description'),
    ]

    def __str__(self):
        return self.name

    def clean(self):
        if self.parent:
            parent = self.parent
            if self.parent == self:
                raise ValidationError('Parent category cannot be self.')
            if parent.parent and parent.parent == self:
                raise ValidationError('Cannot have circular Parents.')

    def save(self, *args, **kwargs):
        if not self.slug:
            unique_slugify(self, self.name)
        return super().save(*args, **kwargs)

    def category_count(self):
        return BlogPageBlogCategory.objects.filter(blog_category_id=self.id).count()



class BlogPageBlogCategory(Orderable):

    class Meta:
        unique_together = ('page', 'blog_category')

    blog_category = models.ForeignKey(
        'BlogCategory',
        related_name="blog_pages",
        on_delete=models.CASCADE,
    )
   
    page = ParentalKey('BlogPage', on_delete=models.CASCADE, related_name='categories')
   
    panels = [
       FieldPanel('blog_category'),
    ]
