from modelcluster.fields import ParentalKey
from taggit.models import TaggedItemBase
from taggit.models import Tag


class BlogTag(Tag):
    class Meta:
        proxy = True


    def tag_count(self):
        return self.wagtail_bs_blog_blogpagetag_items.all().count() 


class BlogPageTag(TaggedItemBase):
    content_object = ParentalKey('BlogPage', related_name='tagged_items')

    class Meta:
        pass
