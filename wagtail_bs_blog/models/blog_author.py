from django.db.models import CharField
from django.db import models 
from django.utils.translation import gettext_lazy as _
from django.utils.html import strip_tags
from wagtail.search import index
from wagtail.models import Page
from wagtail.fields import StreamField
from wagtail.admin.panels import (
    MultiFieldPanel,
    FieldPanel,
    PageChooserPanel
)
from wagtail_bs_blog import settings
from wagtail_bs_blocks.blocks import get_layout_blocks

from wagtail.models import Orderable
from modelcluster.fields import ParentalKey



class BlogAuthor(Page):

    class Meta:
        verbose_name = _('Blog Author')

    parent_page_types = ['BlogIndexPage']
    subpage_types = []

    content = StreamField(
            get_layout_blocks(),
            blank=True,
            null=True,
            use_json_field=True)

    @property
    def get_searchable_content(self):
        return strip_tags(self.content.render_as_block()).replace('\n', ' ').strip()

    search_fields = Page.search_fields + [
            index.SearchField('get_searchable_content'),
            ]

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('content'),
        ], 'Content'),
    ]


# InlinePanel('related_authors', label="Authors"),
class BlogAuthorRelated(Orderable):

    author_page = models.ForeignKey(
        'BlogAuthor',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
    )

    page = ParentalKey('BlogPage', on_delete=models.CASCADE, related_name='related_authors')

    panels = [
        PageChooserPanel('author_page', 'wagtail_bs_blog.BlogAuthor'),
    ]
