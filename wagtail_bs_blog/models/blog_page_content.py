from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.html import strip_tags
from wagtail.search import index
from wagtail.fields import StreamField
from wagtail.admin.panels import (
        TabbedInterface,
        InlinePanel,
        ObjectList,
        FieldPanel,
)
from wagtail.models import Orderable
#django-modelcluster
from modelcluster.fields import ParentalKey
from wagtail_bs_blocks.blocks import get_blocks
from .blog_page import BlogPage


class BlogPageContent(BlogPage):

    class Meta:
        verbose_name = _('Blog Content')

    @property
    def get_contents(self):
        return self.blogpage_contents.order_by('sort_order')

    @property
    def get_searchable_contents(self):
        text = []
        for element in self.get_contents:
            text.append(strip_tags(element.content.render_as_block()).replace('\n', ' ').strip())
        return ' '.join(text)

    search_fields = BlogPage.search_fields + [
        index.SearchField('get_searchable_contents'),
    ]

    edit_handler = TabbedInterface([
        ObjectList(BlogPage.content_panels, heading='Main'),
        ObjectList([InlinePanel('blogpage_contents', label="Contents")], heading='Contents'),
        ObjectList(BlogPage.promote_panels, heading='Promote'),
        ObjectList(BlogPage.settings_panels, heading='Settings', classname="settings"),
        ])


class BlogPageContentOrderable(Orderable):

    page = ParentalKey(BlogPageContent, on_delete=models.CASCADE, related_name='blogpage_contents')

    content = StreamField(get_blocks(), use_json_field=True)

    panels = [
        FieldPanel('content'),
    ]
