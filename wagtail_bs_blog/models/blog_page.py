import datetime
#from django.conf import settings
from django.db.models import CharField
from django.db import models
from django.utils.translation import gettext_lazy as _
from wagtail.search import index
from wagtail.models import Page
from wagtail.admin.panels import (
    FieldPanel,
    InlinePanel,
    MultiFieldPanel,
)
from modelcluster.tags import ClusterTaggableManager
from wagtail_bs_blog import settings


class BlogPage(Page):

    parent_page_types = ['BlogIndexPage']
    subpage_types = []
    tags = ClusterTaggableManager(through='BlogPageTag', blank=True)
    post_date = models.DateField(
        _("Post date"), default=datetime.datetime.today,
        help_text=_("This date may be displayed on the blog post. \
                    It is not used to schedule posts \
                    to go live at a later date.")
    )
    description = CharField(
        max_length=200,
        blank=True,
    )

    search_fields = Page.search_fields + [
        index.SearchField('description'),
        index.FilterField('post_date'),
        index.SearchField('all_authors'),
        index.SearchField('all_categories'),
        index.RelatedFields('tags', [
            index.SearchField('name'),
        ]),
    ]

    content_panels = Page.content_panels + [
        FieldPanel('description'),
        FieldPanel('post_date'),
        InlinePanel('related_authors', label="Authors"),
        FieldPanel('tags'),
        InlinePanel('categories', label=_("Categories")),
    ]

    def all_categories(self):
        categories = [str(c.blog_category) for c in self.categories.all()]
        return ', '.join(categories)

    def all_tags(self):
        tags = [t.name for t in self.tags.all()]
        return ', '.join(tags)

    def all_authors(self):
        authors = [a.author_page.title for a in self.related_authors.all()]
        return ', '.join(authors)

    def get_blog_layout(self):
        return self.get_parent().specific.blog_layout

    def get_blog_index(self):
        return self.get_parent().specific

    # used by templatetags.blog_tags.get_intro
    def get_intro(self):
        return self.description[:settings.INTRO_WORD_COUNT]

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context['COMMENTS_APP'] = getattr(settings, 'COMMENTS_APP', None)
        return context
