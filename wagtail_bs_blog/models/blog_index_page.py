from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils.translation import gettext_lazy as _
from django.shortcuts import get_object_or_404
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail.models import Page
from wagtail.fields import StreamField
from wagtail.admin.panels import (
        TabbedInterface,
        FieldPanel,
        ObjectList
)
from wagtailmenus.models import MenuPageMixin
from wagtailmenus.panels import menupage_panel
from wagtail_bs_blocks.blocks import get_layout_blocks
from wagtail_bs_blog import settings
from .category import BlogCategory, BlogPageBlogCategory
from .tags import BlogTag, BlogPageTag
from .blog_page import BlogPage
from ..blocks import IndexBlocks, BlogBlocks
from .blog_author import BlogAuthor


class BlogIndexPage(RoutablePageMixin, MenuPageMixin, Page):

    class Meta:
        verbose_name = _('Blog Index')
        verbose_name_plural = _('Blog Index')

    template = "wagtail_bs_blog/blog_index_page.html"
    subpage_types = [
        'BlogPageContent',
        'BlogAuthor',
    ]

    # fields used in templates
    blogs = None
    category = None
    tags = None
    author = None

    index_layout = StreamField(
            get_layout_blocks(extra_blocks=[('index_blocks', IndexBlocks()),]),
            use_json_field=True)

    blog_layout = StreamField(
            get_layout_blocks(extra_blocks=[('blog_blocks', BlogBlocks()),]),
            use_json_field=True)

    settings_panels = Page.settings_panels + [
        menupage_panel
    ]
      
    edit_handler = TabbedInterface([
        ObjectList(Page.content_panels, heading='Main'),
        ObjectList([FieldPanel('index_layout')], heading='Index Layout'),
        ObjectList([FieldPanel('blog_layout')], heading='Blog Layout'),
        ObjectList(Page.promote_panels, heading='Promote'),
        ObjectList(settings_panels, heading='Settings', classname="settings"),
        ])

    @property
    def get_blogs(self):
        # Get list of blog pages that are descendants of this page
        blogs = BlogPage.objects.descendant_of(self).live()
        blogs = blogs.order_by(
            '-post_date'
        ).select_related('owner').prefetch_related(
            'tagged_items__tag',
             'categories',
             'categories__blog_category',
        )
        return blogs

    def get_categories(self):
        """
        returns a BlogPageBlogCategory
        blog_category
        """
        relatives = BlogPageBlogCategory.objects.filter(page_id__in=self.blogs.values_list('id', flat=True)).distinct('blog_category_id')
        cats = BlogCategory.objects.filter(id__in=relatives.values_list('blog_category_id', flat=True))
        return cats

    def get_tags(self):
        relatives = BlogPageTag.objects.filter(content_object_id__in=self.blogs.values_list('id', flat=True))
        tags = BlogTag.objects.filter(id__in=relatives.values_list('tag_id', flat=True))
        return tags

    def get_context(self, request, tag=None, *args, **kwargs):
        context = super(BlogIndexPage, self).get_context(
            request, *args, **kwargs)
        # Pagination
        page = request.GET.get('page')

        pagination_per_page = settings.PAGINATION_PER_PAGE 
        paginator = None
        if pagination_per_page:
            paginator = Paginator(self.blogs, pagination_per_page)  # Show 10 blogs per page
            try:
                context['paginator'] = paginator.page(page)
            except PageNotAnInteger:
                context['paginator'] = paginator.page(1)
            except EmptyPage:
                context['paginator'] = paginator.page(paginator.num_pages)
        return context

    @route(r'^$')
    def blog_list(self, request, *args, **kwargs):
        self.blogs = self.get_blogs
        return Page.serve(self, request, *args, **kwargs)

    @route(r"^tag/(?P<tag_slug>[-\w]+)/", name='blogs_by_tag')
    def blogs_by_tag(self, request, tag_slug, *args, **kwargs):
        self.tags = get_object_or_404(BlogTag, slug=tag_slug)
        self.blogs = self.get_blogs.filter(tags__in=[self.tags])
        return Page.serve(self, request, *args, **kwargs)

    @route(r"^category/(?P<cat_slug>[-\w]+)/", name='blogs_by_category')
    def blogs_by_category(self, request, cat_slug, *args, **kwargs):
        self.category = get_object_or_404(BlogCategory, slug=cat_slug)
        self.blogs = self.get_blogs.filter(categories__blog_category__name=self.category)
        return Page.serve(self, request, *args, **kwargs)

    @route(r"^by/(?P<auth_slug>[-\w]*)/$", name='blogs_by_author')
    def blogs_by_author(self, request, auth_slug, *args, **kwargs):
        self.author = get_object_or_404(BlogAuthor, slug=auth_slug)
        self.blogs = self.get_blogs.filter(related_authors__author_page__title__in=[self.author])
        return Page.serve(self, request, *args, **kwargs)

    @route(r"^author/(?P<auth_slug>[-\w]*)/$", name='blog_author')
    def blog_author(self, request, auth_slug, *args, **kwargs):
        self.author = get_object_or_404(BlogAuthor, slug=auth_slug)
        return self.author.serve(request)
