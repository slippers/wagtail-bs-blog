from django.apps import AppConfig


class BlogConfig(AppConfig):
    name = 'wagtail_bs_blog'
    verbose_name = 'Wagtail Boostrap Blog'
    default_auto_field = 'django.db.models.AutoField'
