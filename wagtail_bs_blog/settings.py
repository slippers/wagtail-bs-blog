import sys


class Settings(object):
    defaults = {
        'PAGINATION_PER_PAGE': ('WAGTAIL_BS_BLOG_PAGINATION_PER_PAGE', 10),
        'INTRO_WORD_COUNT': ('WAGTAIL_BS_BLOG_INTRO_WORD_COUNT',50),
    }

    def __getattr__(self, attribute):
        from django.conf import settings
        if attribute in self.defaults:
            return getattr(settings, *self.defaults[attribute])


sys.modules[__name__] = Settings()
