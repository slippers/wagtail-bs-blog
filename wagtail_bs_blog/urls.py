from django.urls import path
from . import views


"""
add to the main.url to include these urls
into to the project.

from wagtail_bs_blog import urls as blog_urls
path('sitenews/', include(blog_urls)),
"""


urlpatterns = [
        path('category/<slug:category>/feed',
            views.LatestCategoryFeed(),
            name="category_feed"),
        path('rss',
            views.LatestEntriesFeed(),
            name="latest_entries_feed"),
        path('atom',
            views.LatestEntriesFeedAtom(),
            name="latest_entries_feed_atom"),
]
