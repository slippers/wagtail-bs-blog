from django.contrib.syndication.views import Feed
from django.utils.feedgenerator import Atom1Feed
from django.shortcuts import get_object_or_404
from django.conf import settings
from .models import BlogIndexPage, BlogPage, BlogCategory

class LatestEntriesFeed(Feed):
    '''
    If a URL ends with "rss" try to find a matching BlogIndexPage
    and return its items.
    '''

    def get_object(self, request):
        return get_object_or_404(BlogIndexPage)

    def title(self, blog):
        if blog.seo_title:
            return blog.seo_title
        return blog.title

    def link(self, blog):
        return blog.full_url

    def description(self, blog):
        return blog.search_description

    def items(self, blog):
        num = getattr(settings, 'BLOG_PAGINATION_PER_PAGE', 10)
        return blog.get_descendants().order_by('-first_published_at')[:num]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.specific.description

    def item_link(self, item):
        return item.full_url

    def item_pubdate(self, blog):
        return blog.first_published_at


class LatestEntriesFeedAtom(LatestEntriesFeed):
    feed_type = Atom1Feed


class LatestCategoryFeed(Feed):
    description = "Blogs by category"

    def get_object(self, request, category):
        return get_object_or_404(BlogCategory, slug=category)

    def title(self, category):
        return "Blogs containing category: " + category.name

    def link(self, category):
        return "/blog/category/" + category.slug

    def item_link(self, blog):
        return blog.url

    def items(self, obj):
        return BlogPage.objects.filter(
            categories__blog_category=obj).order_by('-first_published_at')[:5]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.description
