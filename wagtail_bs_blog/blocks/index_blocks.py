from django.utils.translation import gettext_lazy as _
from wagtail.blocks import StructBlock, StreamBlock, CharBlock
from wagtail_bs_blocks.blocks import TitleHeaderBlock

class IndexBlogCardBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blog/blocks/index_blog_card_block.html'
        icon = 'list-ul'
        help_text = _('Index blog card block')


class IndexBlogListBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blog/blocks/index_blog_list_block.html'
        icon = 'list-ul'
        help_text = _('Index blog list block')


class IndexCatBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blog/blocks/index_cat_block.html'
        icon = 'tag'
        help_text = _('Index blog categories')


"""
embed the header block and pass the page title as text
"""
class IndexHeaderBlock(StructBlock):

    class Meta:
        icon = "title"
        template = 'wagtail_bs_blog/blocks/index_header_block.html'

    header = TitleHeaderBlock()


class IndexNavBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blog/blocks/index_nav_block.html'
        icon = 'home'
        help_text = _('Blog Index Navigation')


class IndexPaginationBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blog/blocks/index_pagination_block.html'
        icon = 'order'
        help_text = _('Blog Index Pagination')


class IndexTagBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blog/blocks/index_tag_block.html'
        icon = 'tag'
        help_text = _('Blog Index Tags')


class IndexBlocks(StreamBlock):
    index_header = IndexHeaderBlock()
    index_blog_list= IndexBlogListBlock()
    index_blog_card = IndexBlogCardBlock()
    index_blog_categories = IndexCatBlock()
    index_blog_tags = IndexTagBlock()
    index_blog_pagination = IndexPaginationBlock()

    class Meta:
        help_text = _('Index Blocks')
