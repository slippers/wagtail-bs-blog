from django.utils.translation import gettext_lazy as _
from wagtail.blocks import StructBlock, StreamBlock
from wagtail_bs_blocks.blocks import TitleHeaderBlock


class BlogNavBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blog/blocks/blog_nav_block.html'
        icon = 'media'
        help_text = _('Blog Navigation')


class BlogHeaderBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blog/blocks/blog_header_block.html'
        icon = 'title'
        help_text = _('Blog Header')

    title = TitleHeaderBlock(field_name='title')
    description = TitleHeaderBlock(field_name='description')


class BlogContentBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blog/blocks/blog_content_block.html'
        icon = 'doc-full'
        help_text = _('Blog Content')


class BlogCommentBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blog/blocks/blog_comment_block.html'
        icon = 'openquote'
        help_text = _('Blog Comments')


class BlogCatBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blog/blocks/blog_cat_block.html'
        icon = 'tag'
        help_text = _('Blog Categories')


class BlogTagBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blog/blocks/blog_tag_block.html'
        icon = 'tag'
        help_text = _('Blog Tags')


class BlogBlocks(StreamBlock):
    blog_header = BlogHeaderBlock()
    blog_content = BlogContentBlock()
    blog_categories = BlogCatBlock()
    blog_tags = BlogTagBlock()
    blog_navigation = BlogNavBlock()

    class Meta:
        help_text = _('Blog Blocks')
