from django.utils.translation import gettext_lazy as _
from wagtail.blocks import StructBlock, StreamBlock
from wagtail_bs_blocks.blocks import TitleHeaderBlock
from .blog_blocks import BlogNavBlock


class AuthorHeaderBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blog/blocks/blog_author_header_block.html'
        icon = 'title'
        help_text = _('Blog Author Header')

    header = TitleHeaderBlock(field_name='author.name')


class AuthorImageBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blog/blocks/blog_author_image_block.html'
        icon = 'image'
        help_text = _('Blog Author Image')


class AuthorAboutBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blog/blocks/blog_author_about_block.html'
        icon = 'doc-full'
        help_text = _('Blog Author About')


class AuthorBlocks(StreamBlock):
    author_header = AuthorHeaderBlock()
    author_image = AuthorImageBlock()
    author_about = AuthorAboutBlock()

    class Meta:
        help_text = _('Author Blocks')
