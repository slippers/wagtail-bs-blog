from django.utils.translation import gettext_lazy as _
from wagtail.snippets.models import register_snippet
from wagtail.snippets.views.snippets import SnippetViewSet
from wagtail.snippets.views.snippets import SnippetViewSetGroup
from .models import (
        BlogCategory,
        BlogTag,
        )


class CategoryAdmin(SnippetViewSet):
    model = BlogCategory
    list_display = ['name', 'slug', 'category_count']
    icon = 'check'


class TagAdmin(SnippetViewSet):
    model = BlogTag
    list_display = ['name', 'slug', 'tag_count']
    icon = 'tag'


class BlogGroup(SnippetViewSetGroup):
    menu_label = _('Blog')
    add_to_admin_menu = True
    icon = 'folder-inverse'
    menu_order = 200 
    items = (CategoryAdmin, TagAdmin, )

register_snippet(BlogGroup)
