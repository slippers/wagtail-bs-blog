# Wagtail Bootstrap Blog

# What is it

A wagtail bootstrap themed blog.

## Features

- Categories tags, authors with views
- RSS
- Basic starter templates with pagination

# Installation

You should start with a existing wagtail django project and have a basic understanding of Wagtail before starting.

using wagtail 2 and higher.

1. `pip install wagtail-bs-blog`
2. Add to INSTALLED_APPS
    wagtail_bs_blog
    wagtail.contrib.settings
    wagtail.contrib.routable_page
3. Add `url(r'^blog/', include('wagtail_bs_blog.urls', namespace="wagtail_bs_blog")),` to urls.py.
add to the top of your url list.
4. `python manage.py migrate`
5. Override [templates](/wagtail_bs_blog/templates/wagtail_bs_blog/) as needed.
